package it.test;

import java.math.BigInteger;

public class Fibonacci {
	
	
	public static BigInteger process(int input) {
		if (input <= 0)
			return BigInteger.ZERO;
		
		if (input == 1 || input == 2)
			return BigInteger.ONE;

		int index = 3;
		BigInteger total = new BigInteger("2");
		BigInteger prev = BigInteger.ONE; 
		while (index < input) {
			BigInteger nextPrev= total;
			total = total.add(prev);
			prev = nextPrev;
			index++;
		}
		return total;
	}
	
	public static int findFibWithDigits(int input) {
		if (input <= 1)
			return 0;

		int index = 3;
		BigInteger total = new BigInteger("2");
		BigInteger prev = BigInteger.ONE; 
		while (total.toString().length() < input) {
			BigInteger nextPrev= total;
			total = total.add(prev);
			prev = nextPrev;
			index++;
		}
		return index;
	}

	public static void main(String[] args) {
		System.out.println(process(7));
		System.out.println(findFibWithDigits(1000));
	}

}
