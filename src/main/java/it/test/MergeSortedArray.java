package it.test;

import java.util.Arrays;

public class MergeSortedArray {

	public static void merge(Integer A[], int m, Integer B[]) {
		int i = m - 1;
		int j = B.length - 1;
		int k = m + B.length - 1;
	 
		while (k >= 0) {
			if (j < 0 || (i >= 0 && A[i] > B[j]))
				A[k--] = A[i--];
			else
				A[k--] = B[j--];
		}
	}
	
	public static void main(String args[]) {
		Integer a[] = {2, 3, 12, 20, 24, 45, 53, 56, 56, 75, 99, null, null, null, null, null, null, null, null };
		Integer b[] = {0, 1, 8, 25, 30, 45, 60, 71};
		MergeSortedArray.merge(a, 11, b);
		Arrays.stream(a).forEach(System.out::println);
	}
}
