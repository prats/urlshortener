package it.test;

@SuppressWarnings("rawtypes")
public class QuickSortComparable {

	public static void quickSort(Comparable[] array) {
		if (array == null || array.length == 0)
			return;

		quickSort(array,  0, array.length- 1);
	}

	@SuppressWarnings("unchecked")
	private static void quickSort(Comparable[] array, int lowerIndex, int higherIndex) {
		if (lowerIndex >= higherIndex)
			return;
 
		// pick the pivot
		int middle = lowerIndex + (higherIndex - lowerIndex) / 2;
		Comparable pivot = array[middle];
 
		// make left < pivot and right > pivot
		int i = lowerIndex, j = higherIndex;
		while (i <= j) {
			while (array[i].compareTo(pivot) < 0)
				i++;
 
			while (array[j].compareTo(pivot) > 0)
				j--;
 
			if (i <= j) {
				Comparable temp = array[i];
				array[i] = array[j];
				array[j] = temp;
				i++;
				j--;
			}
		}
 
		if (lowerIndex < j)
			quickSort(array, lowerIndex, j);
 
		if (higherIndex > i)
			quickSort(array, i, higherIndex);
	}

	public static void main(String a[]) {
		Long[] input = { 24L, 2L, 45L, 20L, 56L, 75L, 3L, 56L, 99L, 53L, 12L };
		QuickSortComparable.quickSort(input);
		for (Long i : input) {
			System.out.print(i);
			System.out.print(" ");
		}
	}
}
