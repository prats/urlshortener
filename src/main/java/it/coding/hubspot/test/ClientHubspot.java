package it.coding.hubspot.test;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.ws.rs.core.MultivaluedMap;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.core.util.MultivaluedMapImpl;

import it.coding.hubspot.test.CountryInvitation;
import it.coding.hubspot.test.CountryInvitationsRequest;
import it.coding.hubspot.test.PartenersResponse;
import it.coding.hubspot.test.Partner;

public class ClientHubspot {
	public static void main(String[] args) throws JsonParseException, JsonMappingException, IOException {
		Client client = Client.create();
		MultivaluedMap<String, String> queryParams = new MultivaluedMapImpl();
		queryParams.add("userKey", "7140c32e09b858eeef213f2c492e");

		WebResource partnerResource = client.resource("https://candidate.hubteam.com/candidateTest/v1/partners");
		ClientResponse response = partnerResource.queryParams(queryParams)
				.header("Content-Type", "application/json;charset=UTF-8").get(ClientResponse.class);

		if (response.getStatus() != 200) {
			throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
		}
		
		// create JSON mapper
		final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.registerModule(new JavaTimeModule());
		objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
		objectMapper.setDateFormat(dateFormat);

		PartenersResponse partnersResponse = null;
		partnersResponse = objectMapper.readValue(response.getEntityInputStream(), PartenersResponse.class);
		
		// map country to partners
		Map<String,List<Partner>> mapCountryPartners = new HashMap<String,List<Partner>>();
		for(Partner partner : partnersResponse.getPartners()) {
			String country = partner.getCountry();
			List<Partner> countryPartners = mapCountryPartners.get(country);
			if (countryPartners == null)
				mapCountryPartners.put(country, countryPartners=new ArrayList<Partner>());
			countryPartners.add(partner);
		}
		
		CountryInvitationsRequest countryInvitationsRequest = new CountryInvitationsRequest();
		for (Entry<String,List<Partner>> entry : mapCountryPartners.entrySet()) {
			String country = entry.getKey();
			List<Partner> partners = entry.getValue();
			
			Map<LocalDate, List<String>> mapCandidateDateAttendees = new HashMap<>();
			// find candidate dates and map these dates to partner
			for (Partner partner : partners) {
				List<LocalDate> partnerAvailableDates = partner.getAvailableDates();
				for (LocalDate availableDate : partnerAvailableDates) {
					if (partnerAvailableDates.contains(availableDate.plusDays(1))) {
						List<String> attendeesByDate = mapCandidateDateAttendees.get(availableDate);
						if (attendeesByDate == null)
							mapCandidateDateAttendees.put(availableDate, attendeesByDate = new ArrayList<>());
						attendeesByDate.add(partner.getEmail());
					}
				}
			}
			
			List<String> countryAttendees = Collections.emptyList();
			LocalDate invitationDate = null;
			if (mapCandidateDateAttendees.size() > 0) {
				Entry<LocalDate, List<String>> bestEntry = mapCandidateDateAttendees.entrySet().stream().min(new DateAttendeeEntryComparator()).get();
				countryAttendees = bestEntry.getValue();
				invitationDate = bestEntry.getKey();
			}
			countryInvitationsRequest.addCountryInvitation(new CountryInvitation(countryAttendees, country, invitationDate));
		}

		String invitationsJSON = objectMapper.writeValueAsString(countryInvitationsRequest);
		WebResource invitationResource = client.resource("https://candidate.hubteam.com/candidateTest/v1/results");
		response = invitationResource.queryParams(queryParams)
								  .header("Content-Type", "application/json;charset=UTF-8").post(ClientResponse.class, invitationsJSON);
		
		System.out.println(response.getEntity(String.class));
		if (response.getStatus() != 200) {
			throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
		}
	}

	private static class DateAttendeeEntryComparator implements Comparator<Entry<LocalDate, List<String>>> {

		@Override
		public int compare(Entry<LocalDate, List<String>> o1, Entry<LocalDate, List<String>> o2) {
			int o1AttendeeSize = o1.getValue().size();
			int o2AttendeeSize = o2.getValue().size();
			if (o2AttendeeSize > o1AttendeeSize )
				return 1;
			else if (o1AttendeeSize == o2AttendeeSize)
				return -o2.getKey().compareTo(o1.getKey());
			else
				return 0;
		}
		
	}
}
