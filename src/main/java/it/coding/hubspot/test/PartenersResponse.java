package it.coding.hubspot.test;

import java.util.List;

public class PartenersResponse {
	private List<Partner> partners;

	public List<Partner> getPartners() {
		return partners;
	}
	public void setPartners(List<Partner> partners) {
		this.partners = partners;
	}
	@Override
	public String toString() {
		return "PartenersResponse [partners=" + partners + "]";
	}
	
}
