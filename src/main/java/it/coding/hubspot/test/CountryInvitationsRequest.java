package it.coding.hubspot.test;

import java.util.ArrayList;
import java.util.List;

public class CountryInvitationsRequest {
	private List<CountryInvitation> countries;
	
	public CountryInvitationsRequest() {
		this.countries = new ArrayList<>();
	}

	public List<CountryInvitation> getCountries() {
		return countries;
	}

	public void addCountryInvitation(CountryInvitation countryInvitation) {
		this.countries.add(countryInvitation);
	}
	
}
