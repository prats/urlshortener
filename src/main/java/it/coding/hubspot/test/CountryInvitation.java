package it.coding.hubspot.test;

import java.time.LocalDate;
import java.util.List;

public class CountryInvitation {
	private Integer attendeeCount;
	private List<String> attendees;
	private String name;
	private LocalDate startDate;
	
	public CountryInvitation(List<String> attendees, String name, LocalDate startDate) { 
		this.attendees = attendees;
		this.name = name;
		this.startDate = startDate;
		this.attendeeCount = attendees == null ? 0 : attendees.size();
	}

	public Integer getAttendeeCount() {
		return attendeeCount;
	}
	public List<String> getAttendees() {
		return attendees;
	}
	public String getName() {
		return name;
	}
	public LocalDate getStartDate() {
		return startDate;
	}

	@Override
	public String toString() {
		return "CountryInvitation [attendeeCount=" + attendeeCount + ", attendees=" + attendees + ", name=" + name
				+ ", startDate=" + startDate + "]";
	}
	
}
