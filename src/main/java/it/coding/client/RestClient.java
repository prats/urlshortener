package it.coding.client;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class RestClient {
	public static void main(String[] args) throws JsonParseException, JsonMappingException, IOException {
		Client client = Client.create();

		WebResource partnerResource = client.resource("http://localhost:8180/UrlShortener/api/public/hello");
		ClientResponse response = partnerResource.get(ClientResponse.class);

		if (response.getStatus() != 200) {
			throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
		}
		
		System.out.println(response.getEntity(String.class));
	}

}
