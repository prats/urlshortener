package it.coding.repository;

public interface IRepository {

	public void put(String id, String url);
	public String get(String id);

}
