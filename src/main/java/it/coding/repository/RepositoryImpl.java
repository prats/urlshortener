package it.coding.repository;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

@Component
public class RepositoryImpl implements IRepository {
	private Map<String,String> repo;
	
	public RepositoryImpl() {
		this.repo = new HashMap<>();
	}
	
	@Override
	public void put(String id, String url) {
		this.repo.put(id, url);
	}
	
	@Override
	public String get(String id) {
		return this.repo.get(id);
	}
}
